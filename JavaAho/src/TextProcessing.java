import java.util.ArrayList;
import java.net.*;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import JavaAho.myFunction;
import org.ahocorasick.interval.*;       
import org.ahocorasick.trie.*;
import java.util.HashMap;
public class TextProcessing {
	//public static LoadPatterns(){

	//}
	public static void main(String[] args) {	
		FileInputStream fis = null;
		myFunction c1 = new myFunction();
		String line="";
		String tmp="";
		int i=0;
		int cnt=0;
		HashMap<String,Integer>hashmap = new HashMap<String,Integer>();
		/* Pre-Biuld the patterns (One time Event) */
		Trie Patterns = c1.ReadPatterns();	
		try {
			BufferedReader reader = new BufferedReader(new FileReader("src/InputText.txt"));
			while ((line = reader.readLine()) != null){
				tmp+=line;
			}
			reader.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		//while(cnt<100){
			long timer = System.currentTimeMillis();
			/* Trigger Java Aho Corasick */	
			Collection<Emit> mylist = c1.Aho_corasick(tmp,Patterns);
			long timer2 = System.currentTimeMillis();

			/*Iterate the Collection */
			Iterator iterator = mylist.iterator(); 
			while (iterator.hasNext()){ 
				String word = iterator.next().toString();
				System.out.println(word); 
			}
			
			System.out.println("Total Time :"+(timer2-timer)/1000.0);
			//cnt++;
		//}
	}
}
