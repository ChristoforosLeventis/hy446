import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.HashMap;
import java.io.*;
import java.util.ArrayList;

public class TextProcessing {


	public static void main(String[] args) {
		

		int count = 0;
		String matchedText = null;
		String line;
		int old_value;

		long startTime;
		long endTime;
		byte[] value;
		int cnt=0;

		File file = new File("src/InputText.txt");
		FileInputStream fileInputStream;
		String filecontent = null;
		try {
			fileInputStream = new FileInputStream(file);
			
			startTime = System.currentTimeMillis();
			value = new byte[(int) file.length()];
			fileInputStream.read(value);	
			endTime = System.currentTimeMillis();
			System.out.println("Bytes :"+(endTime-startTime)/1000.0);
			fileInputStream.close();

			startTime = System.currentTimeMillis();
			filecontent = new String(value, "UTF-8");
			endTime = System.currentTimeMillis();
			System.out.println("Casting to String :"+(endTime-startTime)/1000.0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
