import java.util.concurrent.TimeUnit;
import java.net.*;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class TextProcessing {

	private final long Keep_Instance_Alive;
	private long timer;
	private long timer2;
	private long timer3;
	private int dev_pos;
	private int local_ws;
	private int global_ws;
	private int mapped;
	private String pat_path;
	private int hex_pat;
	private int pat_size_limit;
	private int max_chunk_size;
	private int max_results;
	private int verbose;
	private int id;
	private int thread_no;
	private int DFA;
	private String []Qarray;
	private int [] ResultArray;
	private int EntryID;
	private String [] EntriesKey;
	private String [] DeleteRecords;
	private String [] FilteredRecords;
	private Map<Integer, Integer> HashMap;
	private long InitWindowTimer;
	private double halflife;

	public TextProcessing(){

		this.timer=0;
		this.timer2=0;
		this.timer3=0;
		this.dev_pos=0;
		this.local_ws=1024;
		this.global_ws=32768;
		this.mapped=0;
		this.pat_path="src/patterns.txt";
		this.hex_pat=0;
		this.pat_size_limit=-1;
		this.max_chunk_size=2048;
		this.max_results=64*4;
		this.verbose=1;
		this.id=0;
		this.thread_no=0;
		this.DFA=0;
		this.Qarray=null;
		this.ResultArray=null;
		this.EntryID=1;
		this.EntriesKey=null;
		/* Field to store the C worker instance */
		this.Keep_Instance_Alive=0;
		/* Create the C instance and store it in above field , the assign takes place in C */
		createCobject(dev_pos,local_ws,global_ws,mapped,pat_path,hex_pat,pat_size_limit,max_chunk_size,max_results,verbose,id,thread_no,DFA);
		this.HashMap = new HashMap<>();
		this.halflife=60;
		this.InitWindowTimer=0;
	}

	//Load the native lib
	static{
		try {
			System.loadLibrary("JNI");
		}catch (UnsatisfiedLinkError e) {
			System.err.println("ERROR:Can't find your JNI library");
			System.exit(-1);
		}
	}
	//Declare native functions
	public native int[] workerInit(int max_chunk_size , String[]myarr,int start);

	public native void createCobject(int dev_pos,int local_ws, int global_ws, int mapped, String pat_path, 
			int hex_pat,int pat_size_limit, int max_chunk_size, int max_results, 
			int verbose,int id, int thread_no,int DFA);
	public native void deleteCobject();


	public static void main(String[] args){
		int i =1;
		int k=0;
		int entries_processed=0;
		int sscore=0;
		float sprecentage;
		double jnitime=0;
		int gputrigger=0;
		String line="";	
		List<String> temps = new ArrayList<String>();		
		DecimalFormat df = new DecimalFormat("#.##");
		/* Create our Object */
		TextProcessing MainObject = new TextProcessing();

		long TotalTimer = System.currentTimeMillis();

		try {
			BufferedReader reader = new BufferedReader(new FileReader("src/InputText.txt"));
			while ((line = reader.readLine()) != null){
				String[]words = line.split(" ");
				for(int size=0;size<words.length;size++)
					temps.add(words[size]);
			}

			reader.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}

		MainObject.Qarray = temps.toArray(new String[0]);

		//if(MainObject.Qarray.length>0)
		//	System.out.println("\n\nSample :"+new String(MainObject.Qarray[0])+"\n\n");

		jnitime=0;
		int cnt=0;
		//while(cnt<100){
			while(MainObject.Qarray.length - entries_processed >0){

				MainObject.timer = System.currentTimeMillis();

				/*Trigger GPU-Match*/
				MainObject.ResultArray = MainObject.workerInit(MainObject.max_chunk_size,MainObject.Qarray,entries_processed);

				MainObject.timer2 = System.currentTimeMillis();

				//System.out.println("Retuned Length "+(MainObject.ResultArray.length-1));
				for(k=0;k<MainObject.ResultArray.length-1;k++){

					if(MainObject.HashMap.containsKey(MainObject.ResultArray[k])){					
						int count = MainObject.HashMap.get(MainObject.ResultArray[k]);
						MainObject.HashMap.put(MainObject.ResultArray[k],count+1);
					}
					else{						
						MainObject.HashMap.put(MainObject.ResultArray[k], 1);
					}
				}

				//System.out.println("Entries "+entries_processed);
				entries_processed+=MainObject.ResultArray[k];
				jnitime += (MainObject.timer2-MainObject.timer)/1000.0; 
				gputrigger++;		
			}
			//System.out.println(MainObject.Qarray.length - entries_processed);

			long TotalTimer2 = System.currentTimeMillis();
			System.out.println(jnitime);
			/* Stats */
			/*System.out.println("****************************Stats*****************************");	
			  System.out.println("#GPU triggered 			: "+gputrigger);	
			  System.out.println("Total GPU Process time(sec)  	: "+ jnitime);
			  System.out.println("Total Entries Processed 	: "+entries_processed);
			  System.out.println("Total Time(msec)  		: "+(TotalTimer2-TotalTimer));
			//System.out.println("Sentiment Percentage 		: "+sprecentage+"%");
			System.out.println("**************************End Stats**************************\n\n");
			*/
			entries_processed=0;
			jnitime = (double)0;
			gputrigger=0;
			
			//Debug print	
			System.out.println("****************************File Stats*****************************");		
			System.out.println("File Name: rawtxt");	
			for (Map.Entry<Integer, Integer> entry : MainObject.HashMap.entrySet()) {
			System.out.println("Word with ID "+entry.getKey()+" was found "+entry.getValue()+" times");
			}	    
			System.out.println("**************************End File Stats**************************\n\n");
			
			/* Reset the Hashmap for next file */
			MainObject.HashMap.clear();
			//cnt++;
		//}
		/*Destroy the allocated memory in JNI */
		MainObject.deleteCobject();
	}//main
}//class
