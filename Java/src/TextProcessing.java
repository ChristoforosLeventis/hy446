import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.HashMap;
import java.io.*;
import java.util.ArrayList;

public class TextProcessing {

	public static String[]ReadPatterns(){

		try{
			BufferedReader reader = new BufferedReader(new FileReader("src/patterns.txt"));
			String line="";
			ArrayList<String> tmp = new ArrayList<String>();
			String[] patterns=null;

			while ((line = reader.readLine()) != null){
				tmp.add(line);
				line="";	
			}

			reader.close();

			patterns = new String[tmp.size()];
			tmp.toArray(patterns);

			return patterns;
		}catch (Exception e){
			System.err.format("Exception occurred trying to read file");
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) {
		
		Matcher matcher;
		HashMap<String, Integer> hashmap_matches = new HashMap<String, Integer>();

		String[] keywords = ReadPatterns();

		int count = 0;
		String matchedText = null;
		String line;
		int old_value;

		long startTime;
		long endTime;
		byte[] value;
		int cnt=0;

		File file = new File("src/InputText.txt");
		FileInputStream fileInputStream;
		String filecontent = null;
		try {
			fileInputStream = new FileInputStream(file);
			value = new byte[(int) file.length()];
			fileInputStream.read(value);
			fileInputStream.close();

			filecontent = new String(value, "UTF-8");
			//while(cnt<100){
				startTime = System.currentTimeMillis();
				for(int i=0; i < keywords.length; i++){
					matcher = Pattern.compile(keywords[i]).matcher(filecontent);
					while(matcher.find()){
						matchedText = matcher.group();
						count++;
					}
					//No need to mesure the update
					if (matchedText != null){
					  	if(!hashmap_matches.containsKey(matchedText)){
					  		hashmap_matches.put(matchedText,count);
					  	}else{
					  		old_value = hashmap_matches.get(matchedText);
					  		hashmap_matches.put(matchedText,(count+old_value));
					  	}
					}
					count = 0;
					matchedText = null;
				}

				endTime = System.currentTimeMillis();

			//	cnt++;

				for (HashMap.Entry<String, Integer> entry : hashmap_matches.entrySet()) {
					System.out.println(entry.getKey() + ":" + entry.getValue().toString());
				}
				hashmap_matches.clear();
				System.out.println("Total Time:"+(endTime-startTime)/1000.0);
			//}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
