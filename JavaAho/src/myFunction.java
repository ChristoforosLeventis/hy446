package JavaAho;
import java.util.ArrayList;
import java.net.*;
import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.ahocorasick.interval.*; 	 
import org.ahocorasick.trie.*; 	 
import org.ahocorasick.trie.handler.*; 	 
import org.ahocorasick.util.*;
public class myFunction {

	public static Trie ReadPatterns(){

		try{
			Trie trie; 
			Trie.TrieBuilder trie2 = Trie.builder();

			BufferedReader reader = new BufferedReader(new FileReader("src/TriePatterns.txt"));
			String line="";
			List<String>Patterns = new ArrayList<String>();
			//trie2.onlyWholeWords();
			
			while ((line = reader.readLine()) != null){
				trie2.addKeyword(line);	
			}
			
			reader.close();
			
			trie=trie2.build();
			return trie;
		}catch (Exception e){
			System.err.format("Exception occurred trying to read file");
			e.printStackTrace();
			return null;
		}
	}

	public static Boolean MyContains(String msg,Trie Patterns){	
		try{
			Collection<Emit> emits = Patterns.parseText(msg);
			
			if(emits.size()!=0)
				return true;
			return false;

		}catch (Exception e){
			System.err.format("Exception occurred trying to read file");
			e.printStackTrace();
			return false;
		}


	}

	public static Collection<Emit> Aho_corasick(String PreFinalList,Trie Patterns){
		Collection<Emit> emits=null;
		try {	
			emits = Patterns.parseText(PreFinalList);
	
		}catch(Exception exception){
			exception.printStackTrace();
		}

		return emits;	

	}
}
