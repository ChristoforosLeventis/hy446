#!/bin/bash
#@Author : Christoforos Leventis
#Email   : xristlev@csd.uoc.gr

WD=$(cd "$(dirname "$0")";pwd)

if [ ! -d "${JAVA_HOME}" ]; then
  echo "$0: the JAVA_HOME environment variable is not defined correctly"
  exit 2
fi

JAVA="${JAVA_HOME}/bin/java"
JAVAC="${JAVA_HOME}/bin/javac"
JAVAH="${JAVA_HOME}/bin/javah"

echo "Compiling the sample class.."
"$JAVAC"  -d "${WD}/src" -classpath "${WD}/src" "${WD}/src/myFunction.java"
"$JAVAC"  -d "${WD}/src" -classpath "${WD}/src" "${WD}/src/TextProcessing.java"

echo "Executing TextProcessing ..."
"$JAVA" -Xmx4096m -classpath "${WD}/src" TextProcessing
