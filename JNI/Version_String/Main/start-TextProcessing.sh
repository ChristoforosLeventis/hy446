#!/bin/bash

WD=$(cd "$(dirname "$0")";pwd)

if [ ! -d "${JAVA_HOME}" ]; then
  echo "$0: the JAVA_HOME environment variable is not defined correctly"
  exit 2
fi

JAVA="${JAVA_HOME}/bin/java"
JAVAC="${JAVA_HOME}/bin/javac"
JAVAH="${JAVA_HOME}/bin/javah"

"$JAVAC" -h "${WD}/src/" "${WD}/src/TextProcessing.java"
"$JAVAC" -Xlint:deprecation "${WD}/src/TextProcessing.java"

echo "Executing TextProcessing..."
"$JAVA" -Djava.library.path="${WD}/src/" -Xmx8192m -classpath "$WD/src/"  TextProcessing
