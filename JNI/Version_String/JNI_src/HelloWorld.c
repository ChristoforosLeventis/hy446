#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>
//#include "HelloJNI.h"

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include "acsmx.h"
#include "utils.h"
#include "clutil.h"
#include "ocl_worker.h"
#include "databuf.h"
#include "ocl_aho_match.h"

#define MAX_PAT_SIZE 4096
#define MAX_ALLOC_SIZE 4096

jfieldID getPtrField(JNIEnv *env,jobject obj){
	static jfieldID ptr=0;
	if(!ptr){
		jclass c = (*env)->GetObjectClass(env,obj);

		ptr = (*env)->GetFieldID(env,c,"Keep_Instance_Alive","J");
		(*env)->DeleteLocalRef(env,c);
	}
	return ptr;
}

JNIEXPORT void JNICALL Java_TextProcessing_createCobject(JNIEnv *env,jobject  obj,
		jint dev_pos, jint local_ws, jint global_ws,jint mapped,jstring pat_path, 
		jint hex_pat, jint pat_size_limit,jint max_chunk_size,jint max_results, 
		jint verbose, jint id, jint thread_no, jint DFA){

	const char *c_pat_path = (*env)->GetStringUTFChars(env,pat_path,0);

	struct ocl_worker_ctx * c = (struct ocl_worker_ctx*)malloc(sizeof(struct ocl_worker_ctx));	
	c = ocl_worker_ctx_create(dev_pos);

	if(ocl_worker_ctx_init(c,dev_pos,local_ws,global_ws,
				mapped,(char*)c_pat_path,hex_pat,pat_size_limit,max_chunk_size,max_results,
				verbose,id,thread_no,0,NULL,NULL,DFA)!=0)
		ERRX(1,"Error init_ocl_worker_ctx");

	(*env)->SetLongField(env,obj,getPtrField(env,obj),(jlong)c);

	/* !Important :Avoid memory leak */
	(*env)->ReleaseStringUTFChars(env,pat_path,c_pat_path);
}

JNIEXPORT void JNICALL Java_TextProcessing_deleteCobject(JNIEnv *env, jobject thisObject){
	struct ocl_worker_ctx * ptr = (struct ocl_worker_ctx *) (*env)->GetLongField(env,thisObject,getPtrField(env,thisObject));
	free(ptr);
}

/*
 * prints which file matched which pattern for the given chunk
 */
void Jprint_matches(struct ocl_worker_ctx *ctx, int c_id,int * patterns,int flag){
	int j=0;
	static int len=0;
	j=0;

	if(flag==1){
		len=0;
		return;
	}
	for (j = 0; j < ctx->db->h_results[ctx->db->max_results * c_id] &&
			(j < ctx->db->max_results - 1); j++){

		patterns[len]=ctx->db->h_results[ctx->db->max_results * c_id + 1 + j];
		len++;	
	}
	return;
}


void *cpu_worker(void *worker_ctx,int *report){
	int i;
	struct ocl_worker_ctx *ctx;
	i=0;
	ctx=NULL;

	ctx = (struct ocl_worker_ctx *)worker_ctx;


	if (!ctx)
		ERRX(1, "ERROR: thread has NULL context!\n");

	/* copy the data to the device */
	databuf_copy_host_to_device(ctx->db, ctx->cl.queue);

	/* scan data */
	ocl_aho_match(&(ctx->cl), ctx->db, ctx->acsm, ctx->local_ws);
	
	/* get the results */
	databuf_copy_device_to_host(ctx->db, ctx->cl.queue);
	/* get the total matches */
	for (i = 0; i < ctx->db->chunks; i++) {
		ctx->matches +=
			ctx->db->h_results[ctx->db->max_results * i];

		/* print the patterns found if verbose is on */
		if ((ctx->db->h_results[ctx->db->max_results * i] != 0)
				&& ctx->verbose )
			Jprint_matches(ctx,i,report,0);

		ctx->db->h_results[ctx->db->max_results * i] = 0;

	}
	i=0;
	ctx->rounds++;
	return 0;
}


JNIEXPORT jintArray JNICALL Java_TextProcessing_workerInit(JNIEnv * env, jobject obj,jint max_chunk_size,jobjectArray ArryPats,jint start){

	int i;
	int sum_up;
	unsigned char *rawString;
	int *ReportMatch;
	jsize inLength;
	struct ocl_worker_ctx * c_worker;
	jintArray ReturnCleanArray;
	jstring element; 
	jsize len_element;
	int align_16;
	/* init variables*/
	len_element=0;
	element=NULL;
	i=0;
	sum_up=0;
	align_16=0;
	rawString=NULL;
	ReportMatch=NULL;
	inLength=0;
	c_worker=NULL;
	ReturnCleanArray=NULL;
	c_worker = NULL;

	c_worker = (struct ocl_worker_ctx *)(*env)->GetLongField(env,obj,getPtrField(env,obj));

	inLength = (*env)->GetArrayLength(env,ArryPats);

	for(i = start; i < inLength ; i++){

		element=(jstring)(*env)->GetObjectArrayElement(env,ArryPats, i);

		len_element  =(*env)->GetArrayLength(env,element); 

		rawString = (*env)->GetStringUTFChars(env,element,0);
		if(Jmy_databuf_add_fd(c_worker->db,strlen(rawString),rawString)==-1)
			break;
	}

	/* entries may not fit in GPU */
	//small hack to get going
	inLength=i*110;
	
	/* Store the bytes */
	c_worker->bytes=c_worker->db->bytes;

	/* set the indecies in gpu */
	Jset_buffer(c_worker->db,max_chunk_size);

	/* Rule marching array */
	align_16 = ROUNDUP(inLength,16);
	
	ReportMatch = (int*)calloc(align_16,sizeof(int));
	
	if(!ReportMatch){
		printf("ERROR: ReportMatch not Allocated\n"); 
		exit(-1);
	}
	/* Execute Matcher */
	cpu_worker(c_worker,ReportMatch);

	/* clean enrties = inEntries - Matched */
	sum_up = c_worker->matches;

	/* create new JavaInstanceArray and Avoid error on malloc 0 size array */
	ReturnCleanArray = (*env)->NewIntArray(env,sum_up+1);

	if(!ReturnCleanArray){
		printf("ERROR : ReturnCleanArray\n");
		exit(-1);
	} 

	/*Last spot reserved for total proccessed entries (lines)*/
	ReportMatch[sum_up]=inLength;
	
	(*env)->SetIntArrayRegion(env,ReturnCleanArray,0,sum_up+1,ReportMatch);
	/* clean up */  
	memset(ReportMatch,0,sizeof(int)*align_16);
	c_worker->matches=0; 
	Jprint_matches(NULL,0,NULL,1);
	free(ReportMatch);  
	ReportMatch=NULL;
	rawString=NULL;
	/* Reset the buffers */	
	databuf_clear(c_worker->db,c_worker->cl.queue);	
	
	return ReturnCleanArray;
}

